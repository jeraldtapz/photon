﻿using ExitGames.Client.Photon;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CreateRoom : MonoBehaviour
{
    [SerializeField]
    private Text roomName;

    [SerializeField]
    private Text password;

    [SerializeField]
    private GameObject createRoomProperties;

    /// <summary>
    /// Gets automatically called by Photon if the request for creating a room fails
    /// </summary>
    /// <param name="codeAndMessage"></param>
    private void OnPhotonCreateRoomFailed(object[] codeAndMessage)
    {
        Debug.Log("Create room failed because: " + codeAndMessage[1]);
    }


    /// <summary>
    ///Gets automatically called by Photon if the request for creating a room succeeds
    /// </summary>
    private void OnCreatedRoom()
    {
        RoomManager.Instance.ResetRectTransform();
        Debug.Log("Room was created successfully!");
        ConnectingTextAnimation.Instance.gameObject.transform.SetAsFirstSibling();
        ConnectingTextAnimation.Instance.gameObject.SetActive(false);
    }

}
