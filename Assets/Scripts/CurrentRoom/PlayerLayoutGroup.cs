﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerLayoutGroup : MonoBehaviour
{
    public static PlayerLayoutGroup Instance;

    private PhotonView photonView;


    /// <summary>
    /// This is the prefab for each player listed on the rooms
    /// </summary>
    [SerializeField]
    private GameObject playerListingPrefab;

    private List<PlayerListing> playerListings = new List<PlayerListing>();

    private void Awake()
    {
        Instance = this;
        photonView = GetComponent<PhotonView>();
    }

    /// <summary>
    /// This method gets automatically called by Photon when the master leaves the room and thus the Master Client Switched
    /// </summary>
    /// <param name="newMasterClient"></param>
    /// This is the new PhotonPlayer that Photon assigned to be the new master client
    private void OnMasterClientSwitched(PhotonPlayer newMasterClient)
    {
        RoomManager.Instance.OnClickLeaveRoom();
    }

    /// <summary>
    /// Gets automatically called by Photon on the client that just joined the room
    /// </summary>
    private void OnJoinedRoom()
    {
        RoomManager.Instance.ResetRectTransform();
        ConnectingTextAnimation.Instance.HideLoadingScreen();
        //clears the room list
        foreach (Transform child in transform)
            Destroy(child.gameObject);
        
        //sets the Room UI as last sibling so that it is visible on the screen
        RoomManager.Instance.transform.SetAsLastSibling();
        
        //Gets the list of players from Photon
        PhotonPlayer[] photonPlayers = PhotonNetwork.playerList;

        //Each player gets passed to PlayerJoinedRoom for Instantiation and checking of duplication
        for (int i = 0; i < photonPlayers.Length; i++)
        {
            PlayerJoinedRoom(photonPlayers[i]);
        }


        //Greys out the buttons that can't be pressed if you are not the master
        if (!PhotonNetwork.isMasterClient)
            ButtonDisabler.Instance.DisableButtons();
        else
            ButtonDisabler.Instance.EnableButtons();
    }

    /// <summary>
    /// Gets automatically called by Photon when a new player joins
    /// This will be called on your client if you've connected already and a new one just conneted
    /// </summary>
    /// <param name="player"></param>
    private void OnPhotonPlayerConnected(PhotonPlayer player)
    {
        //Check for duplication and Instantiation
        PlayerJoinedRoom(player);
    }

    /// <summary>
    /// Gets automatically called by Photon when a player disconnected
    /// </summary>
    /// <param name="player"></param>
    /// The PhotonPlayer player is the player that just disconnected
    private void OnPhotonPlayerDisconnected(PhotonPlayer player)
    {
        //Calls this method for destroying the object and removing it from the list of players
        PlayerLeftRoom(player);
    }

    /// <summary>
    /// Checks for duplication, destroys the player if it already exists
    /// Instantiates the player passed
    /// </summary>
    /// <param name="player"></param>
    private void PlayerJoinedRoom(PhotonPlayer player)
    {
        if (player == null)
            return;
        //this is called to make sure there will be no duplicates
        //if PlayerLeftRoom() finds the parameter player on the list, it will be destroyed
        PlayerLeftRoom(player);


        //Instantiates the object
        GameObject playerListingObject = Instantiate(playerListingPrefab);
        //Sets its parent on the GameObject which has a Layout Group
        playerListingObject.transform.SetParent(transform, false);

        PlayerListing playerListing = playerListingObject.GetComponent<PlayerListing>();
        playerListing.ApplyPhotonPlayer(player);
        playerListings.Add(playerListing);
    }

    /// <summary>
    /// Finds and removes the player on the playerList
    /// Destroys the GameObject on the scene
    /// </summary>
    /// <param name="player"></param>
    private void PlayerLeftRoom(PhotonPlayer player)
    {
        //finds the player to be removed on the List<PlayerListings>
        //if it finds it, it will be removed and destroyed
        int index = playerListings.FindIndex(x => x.PhotonPlayer == player);
        if (index != -1)
        {
            Debug.Log("A player has left the room! " + player.NickName);
            Destroy(playerListings[index].gameObject);
            playerListings.RemoveAt(index);
        }
    }

    /// <summary>
    /// Gets called by ChangeName to change the name shown on the player Listings on the right side of the screen
    /// Calls ChangePlayerListingNamesOnAllClients on all the clients including the master
    /// </summary>
    /// <param name="oldName"></param>
    /// <param name="newName"></param>
    public void ChangePlayerListingName(string oldName, string newName)
    {
        if (PhotonNetwork.inRoom)
            photonView.RPC("ChangePlayerListingNamesOnAllClients", PhotonTargets.All, oldName, newName);
    }

    /// <summary>
    /// Finds the oldName on the list of Players and then changes its name to newName
    /// </summary>
    /// <param name="oldName"></param>
    /// <param name="newName"></param>
    [PunRPC]
    private void ChangePlayerListingNamesOnAllClients(string oldName, string newName)
    {
        playerListings.Find(x => x.PlayerName.text == oldName).PlayerName.text = newName;
    }
   

}
