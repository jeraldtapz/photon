﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerListing : MonoBehaviour
{
    public PhotonPlayer PhotonPlayer { get; private set; }
    [SerializeField]
    private Text playerName;
    public Text PlayerName
    {
        get { return playerName; }
        set { playerName = value; }
    }

 
    /// <summary>
    /// Gets the details from the photon Player and copies it to the PlayerListing
    /// </summary>
    /// <param name="photonPlayer"></param>
    public void ApplyPhotonPlayer(PhotonPlayer photonPlayer)
    {
        PhotonPlayer = photonPlayer;
        PlayerName.text = photonPlayer.NickName;
    }

}
