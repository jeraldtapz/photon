﻿//using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CardProperties : MonoBehaviour
{
    private static List<int> currentTextCardIndeces0;
    private static List<int> textIndexPool0;

    private static List<int> currentTextCardIndeces1;
    private static List<int> textIndexPool1;

    private static List<int> currentEffectsCardIndeces;
    private static List<int> effectsIndexPool;

    private static List<int> currentMemeCardIndeces;
    private static List<int> memesIndexPool;

    private static int memeCount0;
    private static int memeCount1;

    public string text;
    public string effect;
    public Sprite meme;
    private bool[] hasBeenInitializedBefore = new bool[2];

    public Vector2 startingPos;
        
    [SerializeField]
    private Text textContent;
    [SerializeField]
    private Image imageContent;
    [SerializeField]
    private Image cardImage;
 
    /// <summary>
    /// Decides whether we should load new values for the cards or retain the old ones
    /// </summary>
    /// <param name="cardType"></param>
    public void Setup()
    {

        if (CardSwitcher.cardType == 0 && !hasBeenInitializedBefore[0])
        {
            //hasBeenInitializedBefore[0] = true;
            InitializeLists();
            SetRandomCardText();
        }
        else if (CardSwitcher.cardType == 1 && !hasBeenInitializedBefore[1])
        {
            //hasBeenInitializedBefore[1] = true;
            InitializeLists();
            SetRandomCardEffect();
        }
        else if(CardSwitcher.cardType == 0)
        {
            SetOldCardText();
        }
        else if(CardSwitcher.cardType == 1)
        {
            SetOldCardEffect();
        }
    }

    /// <summary>
    /// Initializes the values for the indeces and the indecesPool
    /// </summary>
    private void InitializeLists()
    {
        //setting up for text0
        if (currentTextCardIndeces0 == null)
            currentTextCardIndeces0 = new List<int>();
        if (textIndexPool0 == null)
        {
            textIndexPool0 = new List<int>();
            for (int i = 0; i < TextLoader.textLenght0; i++)
                textIndexPool0.Add(i);
        }

        //setting up for text1
        if (currentTextCardIndeces1 == null)
            currentTextCardIndeces1 = new List<int>();
        if (textIndexPool1 == null)
        {
            textIndexPool1 = new List<int>();
            for (int i = 0; i < TextLoader.textLenght1; i++)
                textIndexPool1.Add(i);
        }

        //setting up for effects
        if (currentEffectsCardIndeces == null)
            currentEffectsCardIndeces = new List<int>();
        if(effectsIndexPool == null)
        {
            effectsIndexPool = new List<int>();
            for (int i = 0; i < EffectsLoader.effectsLength; i++)
            {
                effectsIndexPool.Add(i);
            }
        }

        //setting up for memes
        if (currentMemeCardIndeces == null)
            currentMemeCardIndeces = new List<int>();
    }

    /// <summary>
    /// Picks index from pool and set it for use
    /// </summary>
    public void SetRandomCardEffect()
    {
        int index = Random.Range(0, effectsIndexPool.Count);
        SetCardEffectProperties(index);

    }

    /// <summary>
    /// Picks index from pool and set it for use
    /// </summary>
    public void SetRandomCardText()
    {
        int index0 = Random.Range(0, textIndexPool0.Count);
        int index1 = Random.Range(0, textIndexPool1.Count);


        if (memeCount1 > memeCount0)
            SetCardTextProperties(0, index0);
        else if(memeCount0 > memeCount1)
            SetCardTextProperties(1, index1);
        else
        {
            int randomizer = Random.Range(0, 2);
            if(randomizer == 0)
                SetCardTextProperties(0, index0);
            else
                SetCardTextProperties(1, index1);
        }
    }

    /// <summary>
    /// Gets called to setup the current effect card
    /// </summary>
    private void SetOldCardEffect()
    {
        textContent.text = effect;
    }

    /// <summary>
    /// Gets called to setup the current text card
    /// </summary>
    private void SetOldCardText()
    {
        imageContent.color = Color.white * new Vector4(1, 1, 1, 0);
        textContent.color = Color.black;
        textContent.text = text;
    }

    /// <summary>
    /// Gets called for setting up random Effect Cards
    /// </summary>
    /// <param name="index"></param>
    private void SetCardEffectProperties(int index)
    {
        AddIndexToList(effectsIndexPool[index], index, 2);
        textContent.text = EffectsLoader.Instance.GetStringAtLine(currentEffectsCardIndeces[currentEffectsCardIndeces.Count - 1]);
        effect = textContent.text;
    }

    /// <summary>
    /// Gets called for setting up random Text Cards
    /// </summary>
    /// <param name="type"></param>
    /// <param name="index"></param>
    private void SetCardTextProperties(int type, int index)
    {
        hasBeenInitializedBefore[CardSwitcher.cardType] = true;

        if(type == 0)
        {
            AddIndexToList(textIndexPool0[index], index, 0);
            textContent.text = TextLoader.Instance.GetStringAtLine(currentTextCardIndeces0[currentTextCardIndeces0.Count - 1], 0);
            memeCount0++;
        }
        else
        {
            AddIndexToList(textIndexPool1[index], index, 1);
            textContent.text = TextLoader.Instance.GetStringAtLine(currentTextCardIndeces1[currentTextCardIndeces1.Count - 1], 1);
            memeCount1++;
        }

        text = textContent.text;
        imageContent.color = Color.white * new Vector4(1, 1, 1, 0f);
        textContent.color = Color.black;
        

    }

    /// <summary>
    /// Keeps track of which indeces has already been used and unused
    /// </summary>
    /// <param name="indexToAdd"></param>
    /// <param name="indexToRemove"></param>
    /// <param name="type"></param>
    private void AddIndexToList(int indexToAdd, int indexToRemove, int type)
    {
        if(type == 0)
        {
            currentTextCardIndeces0.Add(indexToAdd);
            textIndexPool0.RemoveAt(indexToRemove);
        }
        else if(type == 1)
        {
            currentTextCardIndeces1.Add(indexToAdd);
            textIndexPool1.RemoveAt(indexToRemove);
        }
        else if(type == 2)
        {
            currentEffectsCardIndeces.Add(indexToAdd);
            effectsIndexPool.RemoveAt(indexToRemove);
        }
        else if(type == 3)
        {
            currentMemeCardIndeces.Add(indexToAdd);
            memesIndexPool.RemoveAt(indexToRemove);
        }
        
    }
}
