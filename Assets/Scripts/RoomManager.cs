﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RoomManager : MonoBehaviour
{
    //Variables
    public static RoomManager Instance;

    [SerializeField]
    public Sprite[] avatars;

    /// <summary>
    /// The PhotonView component that is attached to this GameObject
    /// Used for RPCs
    /// </summary>
    [SerializeField]
    private PhotonView photonView;

    /// <summary>
    /// This is the Text UI that displays the name of the player on the room
    /// </summary>
    [SerializeField]
    private Text playerNameTextOnRoom;
    
    /// <summary>
    /// Room Name Text on the room
    /// </summary>
    [SerializeField]
    private Text roomNameTextOnRoom;

    /// <summary>
    /// Room Password Text on the room
    /// </summary>
    [SerializeField]
    private Text passwordTextOnRoom;

    /// <summary>
    /// Avatar Image on the room
    /// </summary>
    [SerializeField]
    private Image avatarOnRoom;

    /// <summary>
    /// Avatar Image on the lobby
    /// </summary>
    [SerializeField]
    private Image avatarOnLobby;

    /// <summary>
    /// This is the Text that shows if the room is Hidden or Visible
    /// </summary>
    [SerializeField]
    private Text roomStateText;

    //Methods
    private void Awake()
    {
        Instance = this;
    }

    /// <summary>
    /// This method is attached to a button that changes visibility of the room to hidden or public
    /// </summary>
    public void OnClickRoomState()
    {
        if (!PhotonNetwork.isMasterClient)
            return;
        PhotonNetwork.room.IsOpen = !PhotonNetwork.room.IsOpen;
        PhotonNetwork.room.IsVisible = PhotonNetwork.room.IsOpen;
        photonView.RPC("OnChangeVisibility", PhotonTargets.All);
    }

    /// <summary>
    /// This method is called on all clients including the master which changes the text on the room UI to either "Public" or "Hidden"
    /// This is called by OnClickRoomState
    /// </summary>
    [PunRPC]
    private void OnChangeVisibility()
    {
        roomStateText.text = ((PhotonNetwork.room.IsVisible) ? ("Public") : "Hidden");
    }

    /// <summary>
    /// This method gets called when you click the Leave button or when the master of the room leaves.
    /// That means you will be forced out of the room when the master of the room leaves.
    /// </summary>
    public void OnClickLeaveRoom()
    {
        ConnectingTextAnimation.Instance.ShowLoadingScreen("Leaving Room");
        PhotonNetwork.LeaveRoom();
        
        //ChangePlayerNames(PhotonNetwork.playerName);
    }

    /// <summary>
    /// This method is attached to the start button in the room.
    /// This will load the appropriate level
    /// </summary>
    public void OnClickStartSync()
    {
        if (PhotonNetwork.isMasterClient)
            PhotonNetwork.LoadLevel(1);
    }

    /// <summary>
    /// This method changes the name displayed on the room when you change your name
    /// </summary>
    /// <param name="name"></param>
    /// This is the string we pass on the methods we use inside the method
    public void ChangePlayerNameOnRoom(string name)
    {
        playerNameTextOnRoom.text = "Player: " + name;
    }

    /// <summary>
    /// Changes the name of the room saved on the Photon Cloud
    /// Sends an RPC to everyone to change their room names on the Room UI
    /// </summary>
    /// <param name="name"></param>
    //public void ChangeRoomNameOnNetwork(string name)
    //{
    //    photonView.RPC("ChangeRoomNameOnRoom", PhotonTargets.All, name);
    //    PhotonNetwork.room.Name = name + "@" + LobbyManager.Instance.Password;
    //}

    /// <summary>
    /// Gets called on every client which changes the name of the room on the Room UI
    /// </summary>
    /// <param name="name"></param>
    [PunRPC]
    public void ChangeRoomNameOnRoom(string name)
    {
        roomNameTextOnRoom.text = "Room: " + name;
    }

    /// <summary>
    /// Changes the password Text on the room UI
    /// </summary>
    /// <param name="password"></param>
    public void ChangePasswordOnRoom(string password)
    {
        passwordTextOnRoom.text = "Password: " + password;
    }

    /// <summary>
    /// Gets called when the Room UI should be shown in the screen
    /// Puts the Room UI in front and puts the Lobby UI on the side
    /// </summary>
    public void ResetRectTransform()
    {
        GetComponent<RectTransform>().localPosition = Vector3.zero;
        LobbyManager.Instance.GetComponent<RectTransform>().localPosition = new Vector3(-1500, 0, 0);
    }

    /// <summary>
    /// Sets the appropriate avatar on the room and the lobby
    /// </summary>
    /// <param name="index"></param>
    public void SetAvatarOnRoomAndLobby(int index)
    {
        avatarOnLobby.sprite = avatars[index];
        avatarOnRoom.sprite = avatars[index];
    }



    
}
