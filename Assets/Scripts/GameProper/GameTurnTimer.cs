﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameTurnTimer : MonoBehaviour
{
    public static GameTurnTimer Instance;

    [SerializeField]
    private Text turnTimer;

    private float timer = 26f;
    public bool isPlayerTurn = false;



	// Use this for initialization
	private void Awake ()
    {
        Instance = this;	
	}

    /// <summary>
    /// Sets the timer if it is your turn.
    /// It is your turn when you are not the meme lord at this turn
    /// </summary>
    private void Update ()
    {
		if(GameManager.Instance.hasStarted)
        {
            if(isPlayerTurn)
            {
                timer -= Time.deltaTime;
                if (timer >= 0)
                    turnTimer.text = "Time: " + ((int)timer).ToString() + " secs";
                else
                {
                    isPlayerTurn = false;
                    //turnTimer.transform.parent.gameObject.SetActive(false);
                    GameManager.Instance.SendAnswersToAll();
                    SetTextToWaitingForPlayers();
                }
            }
            
        }
	}

    public void SetTextToJudging()
    {
        turnTimer.text = "Meme Lord choosing...";
    }

    public void SetTextToWaitingForPlayers()
    {
        turnTimer.text = "Waiting for players...";
    }

    public void SetTextToTimer()
    {
        timer = 26f;
        isPlayerTurn = true;
    }

    public void SetTextToChoose()
    {
        turnTimer.text = "Please choose one...";
    }


}
