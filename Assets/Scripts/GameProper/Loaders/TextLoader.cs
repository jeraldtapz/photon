﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Linq;

public class TextLoader : MonoBehaviour
{
    public static int textLenght0 = 6;
    public static int textLenght1 = 6;
    public static TextLoader Instance;

    [SerializeField]
    private TextAsset textFile0;
    [SerializeField]
    private TextAsset textFile1;

    private void Awake()
    {
        Instance = this;
    }

    /// <summary>
    /// Returns a string from the text file based on index(line)
    /// </summary>
    /// <param name="index"></param>
    /// <param name="type"></param>
    /// <returns></returns>
    public string GetStringAtLine(int index, int type)
    {
        if (type == 0)
            return textFile0.text.Split('\n')[index];
        else
            return textFile1.text.Split('\n')[index];
    }
}
