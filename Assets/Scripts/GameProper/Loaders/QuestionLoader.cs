﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestionLoader : MonoBehaviour
{
    public static int questionsLength = 10;
    public static QuestionLoader Instance;

    [SerializeField]
    private TextAsset questionsText;

	// Use this for initialization
	void Awake ()
    {
        Instance = this;
	}
	
	public string GetStringAtLine(int index)
    {
        return questionsText.text.Split('\n')[index];
    }
}
