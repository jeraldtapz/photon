﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EffectsLoader : MonoBehaviour
{
    public static int effectsLength = 5;
    public static EffectsLoader Instance;

    [SerializeField]
    private TextAsset effectsText;

    private void Awake()
    {
        Instance = this;
    }

    /// <summary>
    /// Returns a string from the text file based on index
    /// </summary>
    /// <param name="index"></param>
    /// <returns></returns>
    public string GetStringAtLine(int index)
    {
        return effectsText.text.Split('\n')[index];
    }
	
}
