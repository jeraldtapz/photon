﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(PhotonView))]
public class QuestionRandomizer : MonoBehaviour
{
    public static QuestionRandomizer Instance;
    [SerializeField]
    private Text questionText;
    private int currentQuestionIndex;
    private PhotonView photonView;
    private List<int> questionsIndexPool;
    
    

    // Use this for initialization
    void Awake ()
    {
        Instance = this;

        questionsIndexPool = new List<int>();
        for (int i = 0; i < QuestionLoader.questionsLength; i++)
        {
            questionsIndexPool.Add(i);
        }
        photonView = GetComponent<PhotonView>();
    }

    public void StartGame()
    {
        SetRandomQuestion();
    }

    /// <summary>
    /// Gets called to set a random question after the 5 second countdown timer has ended
    /// </summary>
    private void SetRandomQuestion()
    {
        int tempIndex = Random.Range(0, questionsIndexPool.Count);
        photonView.RPC("SetRandomQuestionOnAllClients", PhotonTargets.All, tempIndex);
    }

    [PunRPC]
    private void SetRandomQuestionOnAllClients(int tempIndex)
    {
        GameTurnTimer.Instance.isPlayerTurn = !TouchManager.Instance.CheckIfMemeLord();
        if (TouchManager.Instance.CheckIfMemeLord())
            GameTurnTimer.Instance.SetTextToWaitingForPlayers();
        currentQuestionIndex = questionsIndexPool[tempIndex];
        questionText.text = QuestionLoader.Instance.GetStringAtLine(currentQuestionIndex);
        questionsIndexPool.RemoveAt(tempIndex);
    }


}
