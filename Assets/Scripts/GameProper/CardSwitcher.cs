﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CardSwitcher : MonoBehaviour
{
    [SerializeField]
    private Sprite[] cardTypes;
    [SerializeField]
    private Image[] gameCards;

    [SerializeField]
    private Text currentCardTypeText;

    public static int cardType;

    private void Start()
    {
        SetupCardsImageAndContent(cardType);
    }

    /// <summary>
    /// Switches the kind of cards shown on deck
    /// </summary>
	public void SwitchCards()
    {
        if (cardType == cardTypes.Length - 1)
            cardType = 0;
        else
            cardType++;

        if (cardType == 0)
            currentCardTypeText.text = "Text Cards";
        else if (cardType == 1)
            currentCardTypeText.text = "Effects Cards";
        else
            currentCardTypeText.text = "Meme Cards";
        SetupCardsImageAndContent(cardType);
    }

    /// <summary>
    /// Change the image of the card appropriately
    /// Changes the value of the card appropriately
    /// </summary>
    /// <param name="cardType"></param>
    private void SetupCardsImageAndContent(int cardType)
    {
        for (int i = 0; i < gameCards.Length; i++)
        {
           
            if (GameManager.Instance.isCardActive[cardType][i] && !gameCards[i].isActiveAndEnabled)
                gameCards[i].gameObject.SetActive(true);
            
            if(!GameManager.Instance.isCardActive[cardType][i])
                gameCards[i].gameObject.SetActive(false);

            gameCards[i].sprite = cardTypes[cardType];
            CardProperties cp = gameCards[i].gameObject.GetComponent<CardProperties>();
            cp.Setup();

        }
    }
}
