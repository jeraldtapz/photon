﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class TouchManager : MonoBehaviour
{
    public static TouchManager Instance;

    /// <summary>
    /// This stores the original transforms of the cards
    /// </summary>
    [SerializeField]
    public Transform[] cardTransforms;

    [SerializeField]
    private Transform masterCardTransform;
    //[SerializeField]
    public Text mainCardText;
    //[SerializeField]
    public Text mainCardEffect;
    [SerializeField]
    private Image masterCardImage;
    [SerializeField]
    private Text temp;

    private int currentCardIndex;
    private Touch touch;

    private void Awake()
    {
        Instance = this;
    }

    /// <summary>
    /// Gets called every frame
    /// Checks for touches
    /// </summary>
    void Update ()
    {
        //Checks for input
        if (Input.touchCount  > 0)
        {
            touch = Input.GetTouch(0);
            //Check if start of a touch
            if(touch.phase == TouchPhase.Began && !GameManager.Instance.hasMadeMove[CardSwitcher.cardType])
            {
                HandleTouchStart();
            }
            //checks if it is the same touch but has moved
            else if(touch.phase == TouchPhase.Moved && !GameManager.Instance.hasMadeMove[CardSwitcher.cardType])
            {
                    DragCard(touch);                
            }
            //when touch has ended, snap back card to original position or put it in 
            else if(touch.phase == TouchPhase.Ended)
            {
                HandleTouchEnd();
            }
        }
	}

    private void HandleTouchStart()
    {
        CheckIfInsideAnyCard(touch.position);
        cardTransforms[currentCardIndex].gameObject.GetComponent<CardProperties>().startingPos = cardTransforms[currentCardIndex].position;
    }

    private void HandleTouchEnd()
    {
        if (!CheckIfInsideMainCard(touch.position))
        {
            SnapBack(false);
        }
        else if(currentCardIndex != -1)
        {
            if (CardSwitcher.cardType == 0)
            {
                if (!CheckIfMemeLord())
                {
                    //mainCardText.transform.parent.gameObject.SetActive(true);
                    mainCardText.transform.parent.GetComponent<Image>().color = Color.white;
                    mainCardText.color = Color.black;
                    mainCardText.text = cardTransforms[currentCardIndex].gameObject.GetComponent<CardProperties>().text;
                    SnapBack(true);
                }
                else
                    SnapBack(false);

            }
            else if (CardSwitcher.cardType == 1)
            {
                if (!CheckIfMemeLord())
                {
                    //mainCardEffect.transform.parent.gameObject.SetActive(true);
                    mainCardEffect.transform.parent.GetComponent<Image>().color = Color.white;
                    mainCardEffect.color = Color.black;
                    mainCardEffect.text = cardTransforms[currentCardIndex].gameObject.GetComponent<CardProperties>().effect;
                    SnapBack(true);
                }
                else
                    SnapBack(false);

            }
        }
        currentCardIndex = -1;
    }

    public void SampleTouchEnd()
    {
        mainCardText.transform.parent.GetComponent<Image>().color = Color.white;
        mainCardText.color = Color.black;
        mainCardText.text = cardTransforms[0].gameObject.GetComponent<CardProperties>().text;
        SnapBack(true);
    }

    private void SnapBack(bool moveMade)
    {
        //snaps back card to original position
        cardTransforms[currentCardIndex].position =
        new Vector3(cardTransforms[currentCardIndex].gameObject.GetComponent<CardProperties>().startingPos.x,
        cardTransforms[currentCardIndex].gameObject.GetComponent<CardProperties>().startingPos.y,
        0);

        if (moveMade)
        {
            cardTransforms[currentCardIndex].gameObject.SetActive(false);
            GameManager.Instance.isCardActive[CardSwitcher.cardType][currentCardIndex] = false;
            GameManager.Instance.hasMadeMove[CardSwitcher.cardType] = true;
        }
    }


    public bool CheckIfMemeLord()
    {
        return PhotonNetwork.player.ID == GameManager.Instance.GetCurrentPlayer();
    }

    /// <summary>
    /// Checks if the touch is with a card
    /// </summary>
    /// <param name="touch"></param>
    private void CheckIfInsideAnyCard(Vector3 touchPosition)
    {
            currentCardIndex = cardTransforms.ToList().FindIndex(x =>
            touchPosition.x >= x.position.x - 87.5f &&
            touchPosition.x <= x.position.x + 87.5f &&
            touchPosition.y <= x.position.y + 125f);
    }

    private bool CheckIfInsideMainCard(Vector3 touchPosition)
    {
        if (touchPosition.x >= masterCardTransform.position.x - 87.5f &&
            touchPosition.x <= masterCardTransform.position.x + 87.5f &&
            touchPosition.y >= masterCardTransform.position.y - 125 &&
            touchPosition.y <= masterCardTransform.position.y + 125)
            return true;
        return false;
    }

    /// <summary>
    /// Moves the card to the current position of the touch
    /// </summary>
    /// <param name="touch"></param>
    private void DragCard(Touch touch)
    {
        if(currentCardIndex != -1)
            cardTransforms[currentCardIndex].position = touch.position;
    }
}
