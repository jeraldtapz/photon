﻿using System.Collections.Generic;
using UnityEngine;
using ExitGames.Client.Photon;
using UnityEngine.UI;
using System.Linq;
using System.Collections;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;
    public bool allPlayersDone;
    public bool canNowPick;
    public bool hasStarted;
    public bool[] hasMadeMove;
    public bool[][] isCardActive;


    private int playerIndex;
    private int PlayerIndex
    {
        get
        {
            return playerIndex;
        }
        set
        {
            photonView.RPC("SyncCurrentPlayerIndex", PhotonTargets.Others, value);
            playerIndex = value;
        }
    }
    public int winningIndex = -1;
    private int WinningIndex
    {
        get { return winningIndex; }
        set
        {
            photonView.RPC("SyncWinningIndex", PhotonTargets.Others, value);
            winningIndex = value;
        }
    }

    private PhotonView photonView;
    private PhotonPlayer[] players;
    private ExitGames.Client.Photon.Hashtable properties = new ExitGames.Client.Photon.Hashtable();
    private List<Answer> currentAnswers = new List<Answer>();

    [SerializeField]
    private Text[] answersText;
    /// <summary>
    /// This text keeps track of who is the current memelord in the round
    /// </summary>
    [SerializeField]
    private Text memeLord;
    /// <summary>
    /// This text is the text in the Main card that shows the question
    /// </summary>
    /// 
    [SerializeField]
    private Text announcerText;
    
    [SerializeField]
    private Text temp;


    private void Awake()
    {
        Instance = this;
        hasMadeMove = new bool[2];
        isCardActive = new bool[2][];
        for (int i = 0; i < 2; i++)
        {
            isCardActive[i] = new bool[5];
        }

        for (int i = 0; i < 2; i++)
        {
            for (int j = 0; j < 5; j++)
            {
                isCardActive[i][j] = true;
            }
        }
    }

    void Start ()
    {
        photonView = GetComponent<PhotonView>();

        if (!PhotonNetwork.isMasterClient)
            return;
        players = PhotonNetwork.playerList;
        photonView.RPC("SetPlayerList", PhotonTargets.All, players);    
        PlayerIndex = Random.Range(0, players.Length);
        properties.Add("Players", players[PlayerIndex].NickName);
        PhotonNetwork.room.SetCustomProperties(properties);
	}

    /// <summary>
    /// Gets automatically calld when the room properties has been changed
    /// </summary>
    /// <param name="propertiesThatChanged"></param>
    private void OnPhotonCustomRoomPropertiesChanged(ExitGames.Client.Photon.Hashtable propertiesThatChanged)
    {
        memeLord.text = "Meme Lord: " + propertiesThatChanged["Players"] as string;
        SetProperTimerOrText(false);
        currentAnswers.Clear();
        canNowPick = false;
        for (int i = 0; i < 2; i++)
        {
            hasMadeMove[i] = false;
        }
    }

    //Gets called when its time to change turns
    public void OnMoveFinished()
    {
        photonView.RPC("SetCurrentPlayer", PhotonTargets.MasterClient);
    }

    /// <summary>
    /// Gets called when the meme lord has selected a winner
    /// </summary>
    /// <param name="index"></param>
    public void OnAnswerSelected(int index)
    {
        WinningIndex = index;
        if(canNowPick && TouchManager.Instance.CheckIfMemeLord())
        {
            photonView.RPC("SetWinner", PhotonTargets.All, currentAnswers[WinningIndex].answer);
        }
    }

    [PunRPC]
    private void SetWinner(string winnerText)
    {
        Debug.Log("Setting up winner");
        if (WinningIndex == -1)
            SetWinner(winnerText);
        else
        {
            Debug.Log(currentAnswers.Count + " : " + currentAnswers[0].answer);
            temp.text = PhotonNetwork.player.ID + " : " + currentAnswers.Find(x => x.answer.Equals(currentAnswers[WinningIndex].answer)).sender.ID;
            for (int i = 0; i < currentAnswers.Count; i++)
            {
                if (currentAnswers[i].answer != winnerText)
                {
                    
                    HideCard(i);
                }
            }
            StartCoroutine(ShowWinner());
        }
    }

    public void SendAnswersToAll()
    {
        photonView.RPC("GatherAnswers", PhotonTargets.All, TouchManager.Instance.mainCardText.text, TouchManager.Instance.mainCardEffect.text, PhotonNetwork.player);
    }

    private void ShowAnswersToAll()
    {
        for (int i = 0; i < currentAnswers.Count; i++)
        {
            ShowCard(i);
        }
        photonView.RPC("SetProperTimerOrText", PhotonTargets.All, true);
    }


    [PunRPC]
    private void GatherAnswers(string text, string effect, PhotonPlayer player)
    {
        Answer answer = new Answer(text, effect, player);
        currentAnswers.Add(answer);


        //tentative condition
        if (currentAnswers.Count == PhotonNetwork.room.PlayerCount - 1)
        {
            temp.text += "A";
            ShowAnswersToAll();
            canNowPick = true;
        }
    }

    [PunRPC]
    private void SetProperTimerOrText(bool allPlayersDone)
    {
        this.allPlayersDone = allPlayersDone;
        if (TouchManager.Instance.CheckIfMemeLord())
        {
            if (allPlayersDone)
                GameTurnTimer.Instance.SetTextToChoose();
            else
                GameTurnTimer.Instance.SetTextToWaitingForPlayers();
        }
        else
        {
            if(allPlayersDone)
                GameTurnTimer.Instance.SetTextToJudging();
            else
                GameTurnTimer.Instance.SetTextToTimer();
        }
    }

    [PunRPC]
    private void SetCurrentPlayer()
    {
        PlayerIndex = players.ToList().FindIndex(x => x.NickName == properties["Players"] as string);
        PlayerIndex++;
        if (PlayerIndex == players.Length)
            PlayerIndex = 0;
        properties["Players"] = players[PlayerIndex].NickName;
        PhotonNetwork.room.SetCustomProperties(properties);
    }

    [PunRPC]
    private void SyncCurrentPlayerIndex(int index)
    {
        playerIndex = index;
    }

    [PunRPC]
    private void SyncWinningIndex(int index)
    {
        winningIndex = index;
    }
    [PunRPC]
    private void SetPlayerList(PhotonPlayer[] players)
    {
        this.players = players;
    }

    public int GetCurrentPlayer()
    {
        return players[PlayerIndex].ID;
    }

    IEnumerator ShowWinner()
    {
        PhotonPlayer player = currentAnswers.Find(x => x.answer == currentAnswers[WinningIndex].answer).sender;
        if (PhotonNetwork.player.ID == player.ID)
            IncrementScore(10);
        announcerText.text = "<size=250><color=green>" + player.NickName + "</color></size>\nwon the round";
        announcerText.gameObject.SetActive(true);
        yield return new WaitForSeconds(3f);
        announcerText.gameObject.SetActive(false);
        HideCard(WinningIndex);
        if (!TouchManager.Instance.CheckIfMemeLord())
        {
            int index = isCardActive[CardSwitcher.cardType].ToList().FindIndex(x => !x);
            GameObject cardToBeSetActive = TouchManager.Instance.cardTransforms[index].gameObject;
            isCardActive[CardSwitcher.cardType][index] = true;
            cardToBeSetActive.SetActive(true);
            if (CardSwitcher.cardType == 0)
                cardToBeSetActive.GetComponent<CardProperties>().SetRandomCardText();
            else
                cardToBeSetActive.GetComponent<CardProperties>().SetRandomCardEffect();
        }
        if (PhotonNetwork.isMasterClient)
            OnMoveFinished();

        
    }

    private void HideCard(int index)
    {
        answersText[index].transform.parent.GetComponent<Button>().interactable = false;
        answersText[index].transform.parent.GetComponent<Image>().color *= new Vector4(1, 1, 1, 0);
        answersText[index].color *= new Vector4(1, 1, 1, 0);
        answersText[index].text = "NULL";
    }

    private void ShowCard(int index)
    {
        answersText[index].transform.parent.GetComponent<Button>().interactable = true;
        answersText[index].transform.parent.GetComponent<Image>().color = Color.white;
        answersText[index].color = Color.black;
        answersText[index].text = currentAnswers[index].answer;
    }

    private void IncrementScore(int scoreAmount)
    {
        ScoreManager.Instance.SetScore(scoreAmount);
        ScoreManager.Instance.Reset();
    }


}

