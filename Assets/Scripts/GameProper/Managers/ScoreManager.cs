﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour
{
    public static ScoreManager Instance;

    [SerializeField]
    private Text scoreText;
    private int scoreMultiplier = 1;
    private int scoreAdditions;
    private int score;

    public int ScoreMultiplier
    {
        get { return scoreMultiplier; }
        set { scoreMultiplier = value; }
    }
    private int ScoreAdditions
    {
        get{ return scoreAdditions; }
        set { scoreAdditions = value; }
    }
    
    public int Score
    {
        get { return score; }
        set
        {
            scoreText.text = "Score:\n" +  value.ToString();
            score = value;
        }
    }

    // Use this for initialization
    void Awake ()
    {
        Instance = this;
	}

    public void SetScore(int scoreAmount)
    {
        Score += scoreAmount*ScoreMultiplier;
    }

    public void Reset()
    {
        ScoreAdditions = 0;
        ScoreMultiplier = 1;
    }


}
