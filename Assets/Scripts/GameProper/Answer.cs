﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Answer
{
    public string answer;
    public PhotonPlayer sender;
    public string effect;

    public Answer(string answer, string effect, PhotonPlayer sender)
    {
        this.answer = answer;
        this.effect = effect;
        this.sender = sender;
    }
}
