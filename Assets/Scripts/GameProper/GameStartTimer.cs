﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameStartTimer : MonoBehaviour
{
    [SerializeField]
    private Text gameStartText;

    private float timer;
    private float startTime = 5f;

	// Use this for initialization
	void Start ()
    {
        gameStartText.gameObject.SetActive(true);
	}

	/// <summary>
    /// This shows the timer for when the game is about to begin
    /// </summary>
	void Update ()
    {
		if(!GameManager.Instance.hasStarted)
        {
            timer += Time.deltaTime;
            if (timer >= 1f && startTime - 1 >= 0)
            {
                
                gameStartText.text = "Game starts in\n<size=300 ><color=red> " + (--startTime).ToString() + "</color></size>";
                timer = 0f;

                if(startTime <= 0)
                {
                    GameManager.Instance.hasStarted = true;
                    if(PhotonNetwork.isMasterClient)
                        QuestionRandomizer.Instance.StartGame();
                    gameStartText.gameObject.SetActive(false);
                }
            }
        }
	}
}
