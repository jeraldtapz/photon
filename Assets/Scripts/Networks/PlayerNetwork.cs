﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.IO;

public class PlayerNetwork : MonoBehaviour
{
    public static PlayerNetwork Instance;
    private PhotonView photonView;


    public string Name;

    /// <summary>
    /// This is the variable that keeps track of how many players has already loaded the game
    /// </summary>
    private int playersInGame = 0;

    /// <summary>
    /// Initializes the static instance
    /// Initializes the PhotonView variable;
    /// </summary>
    private void Awake()
    {
        Instance = this;

        //Sets the PlayerName to a saved name on the PlayerPrefs. If there is none, it will default
        //PlayerName = (PlayerPrefs.HasKey("PlayerName")) ? PlayerPrefs.GetString("PlayerName") : "DefaultName";
        photonView = GetComponent<PhotonView>();


        //Registers a callback to when a scene has just finished loading
        SceneManager.sceneLoaded += OnSceneFinishedLoading;

        PhotonNetwork.sendRate = 60;
        PhotonNetwork.sendRateOnSerialize = 30;
    }

    /// <summary>
    /// Used for initialization
    /// Checks for the name stored locally and sets the name accordingly
    /// Checks for the avatar stored locally and sets the name accordingly
    /// </summary>
    private void Start()
    {
        //checks if there is stored name on the disk
        if (PlayerPrefs.HasKey(ChangePlayerName.NamePlayerPrefsKey))
        {
            Name = PlayerPrefs.GetString(ChangePlayerName.NamePlayerPrefsKey);
            LobbyManager.Instance.ChangePlayerNameOnLobby(Name);
            RoomManager.Instance.ChangePlayerNameOnRoom(Name);
        }

        //checks if there is a stored avatar on the disk
        if (PlayerPrefs.HasKey("Avatar"))
            RoomManager.Instance.SetAvatarOnRoomAndLobby(PlayerPrefs.GetInt("Avatar"));
        else
            RoomManager.Instance.SetAvatarOnRoomAndLobby(Random.Range(0, RoomManager.Instance.avatars.Length));
    }
    

    /// <summary>
    /// Gets called automatically by Unity through sceneLoaded event when the scene has just finished loading
    /// </summary>
    /// <param name="scene"></param>
    /// <param name="mode"></param>
    private void OnSceneFinishedLoading(Scene scene, LoadSceneMode mode)
    {
        if(scene.name == "Level 0")
        {
            if (PhotonNetwork.isMasterClient)
                MasterLoadedGame();
            else
                NonMasterLoadedGame();
        }
    }

    /// <summary>
    /// Gets called if it is the master that has loaded the scene
    /// </summary>
    private void MasterLoadedGame()
    {
        photonView.RPC("RPC_LoadedGameScene", PhotonTargets.MasterClient);
        photonView.RPC("RPC_LoadGameOthers", PhotonTargets.Others);
        
    }

    /// <summary>
    /// Gets called if it is not the master that has loaded the scene
    /// </summary>
    private void NonMasterLoadedGame()
    {
        photonView.RPC("RPC_LoadedGameScene", PhotonTargets.MasterClient);
    }


    /// <summary>
    /// Gets called by the master client to instruct other clients to load their respective scenes
    /// </summary>
    [PunRPC]
    private void RPC_LoadGameOthers()
    {
        PhotonNetwork.LoadLevel(1);
    }

    /// <summary>
    /// Gets called when master or non master client has loaded their scene 
    /// </summary>
    [PunRPC]
    private void RPC_LoadedGameScene()
    {
        playersInGame++;
        //Only if all players are now in the scene, that we'll instantiate a player
        if (playersInGame == PhotonNetwork.playerList.Length)
        {
            Debug.Log("All players are now in the game scene");
        }
    }
        
    //[PunRPC]
    //private void RPC_CreatePlayer()
    //{
    //    float randomValue = Random.Range(0f, 5f);
    //    //PhotonNetwork.Instantiate(Path.Combine("Prefabs", "NewPlayer"), Vector3.up * randomValue, Quaternion.identity, 0);
    //}
}
