﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LobbyNetwork : MonoBehaviour
{
    /// <summary>
    /// This is the UI that shows the loading animations
    /// </summary>
    [SerializeField]
    private GameObject loadingPanel;

    
    [SerializeField]
    private ConnectingTextAnimation connectingTextAnimation;


    /// <summary>
    /// Connects to the Photon Cloud at the start of the game
    /// </summary>
	void Start ()
    {
        //loadingPanel.SetActive(true);
        ConnectingTextAnimation.Instance.ShowLoadingScreen("Connecting to Server");
        Debug.Log("Connecting to server");
        //Used to connect to the Photon Cloud
        PhotonNetwork.ConnectUsingSettings("0.0.0");
	}

    /// <summary>
    /// Gets automatically called when the player has connected to the Photon Cloud successfully
    /// </summary>
    private void OnConnectedToMaster()
    {
        Debug.Log("Connected to Master");

        //this is to make sure that Photon will sync the scene on each client
        PhotonNetwork.automaticallySyncScene = true;

        //sets the name to the saved name or sets it to default
        
        PhotonNetwork.playerName = PlayerNetwork.Instance.Name;

        //Tells Photon to join this client to the lobby
        PhotonNetwork.JoinLobby(TypedLobby.Default);
    }
	
    /// <summary>
    /// Gets called when the client has successfully joined the lobby
    /// </summary>
    private void OnJoinedLobby()
    {
        ConnectingTextAnimation.Instance.HideLoadingScreen();
        if (!PhotonNetwork.inRoom)
            LobbyManager.Instance.gameObject.transform.SetAsLastSibling();
        Debug.Log("Joined the Lobby!");
        LobbyManager.Instance.ResetRectTransform();
    }

    
}
