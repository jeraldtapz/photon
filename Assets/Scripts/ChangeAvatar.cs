﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ChangeAvatar : MonoBehaviour
{
    /// <summary>
    /// The list of sprites which represents the avatars
    /// These are set in the editor
    /// </summary>
    [SerializeField]
    private Sprite[] avatars;

    /// <summary>
    /// This is the UI that pops up when you are changing your avatar
    /// </summary>
    [SerializeField]
    private GameObject changeAvatarProperties;

    [SerializeField]
    private Image avatarImageOnLobby;
    [SerializeField]
    private Image avatarImageOnRoom;

    /// <summary>
    /// This is the index that represents the player's current avatar
    /// </summary>
    private int currentIndex = -1;

    /// <summary>
    /// Gets called when you click the Avatar
    /// This will show the Change Avatar UI
    /// </summary>
    public void OnClickChangeAvatar()
    {
        changeAvatarProperties.SetActive(true);
        changeAvatarProperties.transform.SetAsLastSibling();
    }

    /// <summary>
    /// Gets called when you click the exit button on the Change Avatar UI
    /// Hides the Change Avatar UI and then disables it in the meantime
    /// </summary>
    public void OnClickExitChangeAvatar()
    {
        changeAvatarProperties.transform.SetAsFirstSibling();
        changeAvatarProperties.SetActive(false);
    }

    /// <summary>
    /// Receives an index and sets the avatar accordingly
    /// </summary>
    /// <param name="index"></param>
    public void GetAvatar(int index)
    {
        if(index == currentIndex)
        {
            SetAvatar(index);
            currentIndex = -1;
            OnClickExitChangeAvatar();
            PlayerPrefs.SetInt("Avatar", index);
        }
        else
            currentIndex = index;
    }

    /// <summary>
    /// Sets the avatar on both images in the lobby and in the room
    /// </summary>
    /// <param name="index"></param>
    private void SetAvatar(int index)
    {
        avatarImageOnLobby.sprite = avatars[index];
        avatarImageOnRoom.sprite = avatars[index];
    }
}
