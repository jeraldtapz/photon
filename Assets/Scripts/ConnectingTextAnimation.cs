﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ConnectingTextAnimation : MonoBehaviour
{
    public static ConnectingTextAnimation Instance;


    private float time;
    private string message;

    /// <summary>
    /// This is the text inside this GameObject
    /// </summary>
    [SerializeField]
    private Text connectingToServerText;

    private void Awake()
    {
        Instance = this;
    }

    /// <summary>
    /// Gets called every frame
    /// changes the dots in the loading screen
    /// </summary>
    void Update ()
    {
        time += Time.deltaTime;
        if (time >= 0.67f && time <= 1.0f)
        {
            connectingToServerText.text = message + "...";
        }
        else if (time >= 0.33f && time < 0.67f)
        {
            connectingToServerText.text = message + "..";
        }
        else if (time >= 0f && time < 0.33f)
        {
            connectingToServerText.text = message + ".";
        }
        else
            time = 0f;
	}

    
    /// <summary>
    /// Shows loading screen with appropriate text
    /// </summary>
    /// <param name="message"></param>
    public void ShowLoadingScreen(string message)
    {
        ConnectingTextAnimation.Instance.gameObject.SetActive(true);
        this.message = message;
        GetComponent<RectTransform>().localPosition = Vector3.zero;
        gameObject.transform.SetAsLastSibling();
    }


    /// <summary>
    /// Hides loading screen and setting it inactive
    /// </summary>
    public void HideLoadingScreen()
    {
        ConnectingTextAnimation.Instance.gameObject.transform.SetAsFirstSibling();
        ConnectingTextAnimation.Instance.gameObject.SetActive(false);
    }
}
