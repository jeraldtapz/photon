﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonDisabler : MonoBehaviour
{
    public static ButtonDisabler Instance;

    /// <summary>
    /// The buttons to be disabled
    /// </summary>
    [SerializeField]
    private Button[] buttons;

    /// <summary>
    /// Used for initialization
    /// </summary>
    private void Awake()
    {
        Instance = this;
    }

    /// <summary>
    /// Disables each button in the array
    /// </summary>
    public void DisableButtons()
    {
        foreach (Button button in buttons)
        {
            button.interactable = false;
        }
    }

    /// <summary>
    /// Enables each button in the array
    /// </summary>
    public void EnableButtons()
    {
        foreach (Button button in buttons)
        {
            button.interactable = true;
        }
    }
}
