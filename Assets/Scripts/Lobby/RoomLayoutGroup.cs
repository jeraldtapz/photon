﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomLayoutGroup : MonoBehaviour
{
    public static RoomLayoutGroup Instance;

    /// <summary>
    /// The prefab for the Room Listing
    /// </summary>
    [SerializeField]
    private GameObject roomListingPrefab;


    /// <summary>
    /// The list of roomListings 
    /// </summary>
    private List<RoomListing> roomListingButtons = new List<RoomListing>();


    private void Awake()
    {
        Instance = this;
    }

    /// <summary>
    /// Gets automatically called by Photon when a there is an update on the list of rooms
    /// Gets the list of rooms and pass each of them to RoomReceived method for proper instantiation and checking of duplicates
    /// </summary>
    private void OnReceivedRoomListUpdate()
    {
        RoomInfo[] rooms = PhotonNetwork.GetRoomList();
        foreach (RoomInfo room in rooms)
        {
            RoomReceived(room);
        }

        RemoveOldRooms();
    }

    /// <summary>
    /// Checks for duplicates in the list. Deletes it if there is.
    /// Instantiates the RoomListing
    /// </summary>
    /// <param name="room"></param>
    private void RoomReceived(RoomInfo room)
    {
        //search on the list using linq
        int index = roomListingButtons.FindIndex(x => x.RoomName == room.Name);
        if(index == -1)
        {
            if(room.IsVisible && room.PlayerCount < room.MaxPlayers)
            {
                GameObject roomListingObj = Instantiate(roomListingPrefab);
                roomListingObj.transform.SetParent(transform, false);

                RoomListing roomListing = roomListingObj.GetComponent<RoomListing>();
                roomListingButtons.Add(roomListing);

                index = roomListingButtons.Count - 1;
            }
        }
        if(index != -1)
        {
            RoomListing roomListing = roomListingButtons[index];
            roomListing.SetRoomNameText(room.Name);
            roomListing.Updated = true;
        }
       
    }

    /// <summary>
    /// Removes old rooms which were not updated on the last check of the list of rooms
    /// </summary>
    private void RemoveOldRooms()
    {
        List<RoomListing> removeRooms = new List<RoomListing>();

        foreach (RoomListing roomListing in roomListingButtons)
        {
            if (!roomListing.Updated)
            {
                removeRooms.Add(roomListing);
            }
            else
                roomListing.Updated = false;
        }

        foreach (RoomListing roomListing in removeRooms)
        {
            GameObject roomListingObj = roomListing.gameObject;
            roomListingButtons.Remove(roomListing);
            Destroy(roomListingObj);
        }
    }
}
