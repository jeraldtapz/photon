﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;



public class LobbyManager : MonoBehaviour
{
    public static LobbyManager Instance;
    public string Password;

    /// <summary>
    /// This is the Input Password UI that gets shown when you click a room to join
    /// </summary>
    [SerializeField]
    private GameObject inputPassword;

    /// <summary>
    /// This is the text the user typed when creating a room
    /// </summary>
    [SerializeField]
    private Text passwordTextCreate;

    /// <summary>
    /// This is where we store the name of the room that we will join
    /// </summary>
    private string roomToJoin;

    /// <summary>
    /// This is the Text UI for the player name on the Lobby 
    /// </summary>
    [SerializeField]
    private Text playerNameOnLobby;

    /// <summary>
    /// This is the text that the user typed for the room name
    /// </summary>
    [SerializeField]
    private Text roomName;

    /// <summary>
    /// This is the text the user typed when entering a room
    /// </summary>
    [SerializeField]
    private Text passwordTextEnter;

    /// <summary>
    /// This is the UI that shows up when you are to create a room
    /// </summary>
    [SerializeField]
    private GameObject createRoomProperties;

    /// <summary>
    /// This is the Text that shows if the room is Hidden or Visible
    /// </summary>
    [SerializeField]
    private Text roomStateText;




    private void Awake()
    {
        Instance = this;
    }

    /// <summary>
    /// Gets called when you click the create room button in the lobby
    /// It activates the UI for entering the name and password for the room
    /// </summary>
    public void OnClickCreateRoomButton()
    {
        createRoomProperties.SetActive(true);
        createRoomProperties.transform.SetAsLastSibling();
    }

    /// <summary>
    /// Gets called when you are done typing the name and password for the room and you've clicked the checkmark which indicates DONE
    /// </summary>
    public void OnClickCreateRoomDoneButton()
    {
        ConnectingTextAnimation.Instance.ShowLoadingScreen("Creating Room");

        //Create a room option object
        RoomOptions roomOptions = new RoomOptions() { IsVisible = true, IsOpen = true, MaxPlayers = 4 };

        if (PhotonNetwork.CreateRoom((roomName.text + "@" + passwordTextCreate.text), roomOptions, TypedLobby.Default))
        {
            roomStateText.text = "Public";
            RoomManager.Instance.ChangeRoomNameOnRoom(roomName.text);
            RoomManager.Instance.ChangePasswordOnRoom(passwordTextCreate.text);
        }
        else
            Debug.Log("Create Room request failed to send");

        OnClickCancelCreateRoomButton();
    }

    /// <summary>
    /// Gets called when you click the X mark on the UI for creating a room
    /// Just hides the create room UI and deactivates it
    /// </summary>
    public void OnClickCancelCreateRoomButton()
    {
        createRoomProperties.transform.SetAsFirstSibling();
        createRoomProperties.SetActive(false);
    }

    /// <summary>
    /// This method is attached to the button RoomListing.
    /// The list of rooms on the right are buttons and this method is attached to those.
    /// </summary>
    /// <param name="roomName"></param>
    /// 
	public void OnClickJoinRoom(string roomName)
    {
        roomToJoin = roomName;
        inputPassword.SetActive(true);
        inputPassword.transform.SetAsLastSibling();
    }

    /// <summary>
    /// This method parses the room name on the network which is in the format RoomName@Password
    /// This method finds the room in the list of rooms and parses it to check if the password matches 
    /// </summary>
    public void CheckIfPasswordMatch()
    {
        RoomInfo[] roomInfo = PhotonNetwork.GetRoomList();
        Debug.Log("Room to join is " + roomToJoin);
        Password = roomInfo.ToList().Find(x => x.Name == roomToJoin).Name.Split('@')[1];
        
        if (Password == passwordTextEnter.text)
        {
            inputPassword.transform.SetAsFirstSibling();
            inputPassword.SetActive(false);
            ConnectingTextAnimation.Instance.ShowLoadingScreen("Joining room");
            if (PhotonNetwork.JoinRoom(roomToJoin))
            {
                Debug.Log("The room name you just joined is: " + roomToJoin);
                RoomManager.Instance.ChangeRoomNameOnRoom(roomToJoin.Split('@')[0]);
                RoomManager.Instance.ChangePasswordOnRoom(Password);
            }
            else
                Debug.Log("Failed to join room" + roomToJoin);
        }
        else
        {
            Debug.Log("Incorrect Password!");
        }
        
    }

    /// <summary>
    /// This method changes the text displayed on the room
    /// This gets called in RoomManager
    /// </summary>
    /// <param name="name"></param>
    /// This is the string we set to the Text UI on the lobby
    public void ChangePlayerNameOnLobby(string name)
    {
        playerNameOnLobby.text = name;
    }

    /// <summary>
    /// Gets called when the Lobby UI should be shown in the screen
    /// Puts the Lobby UI in front and puts the Room UI on the side
    /// </summary>
    public void ResetRectTransform()
    {
        GetComponent<RectTransform>().localPosition = Vector3.zero;
        RoomManager.Instance.GetComponent<RectTransform>().localPosition = new Vector3(-1500, 0, 0);
    }
}
