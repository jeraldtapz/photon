﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RoomListing : MonoBehaviour
{
    /// <summary>
    /// This is the 
    /// </summary>
    [SerializeField]
    private Text roomNameText;
   

    public string RoomName { get;  set; }
    public string Password { get; private set; }

    public bool Updated { get; set; }

	void Start ()
    {
        GameObject lobbyManagerObject = LobbyManager.Instance.gameObject;
        if (lobbyManagerObject == null)
            return;

        LobbyManager lobbyManager = lobbyManagerObject.GetComponent<LobbyManager>();
        Button button = GetComponent<Button>();
        button.onClick.AddListener(() => lobbyManager.OnClickJoinRoom(roomNameText.text+"@"+Password));

    }

    
    /// <summary>
    /// Gets called by Unity when the GameObject that this script is attached to gets destroyed
    /// </summary>
    private void OnDestroy()
    {
        Button button = GetComponent<Button>();
        button.onClick.RemoveAllListeners();
    }

    /// <summary>
    /// Sets the Text UI(child of this prefab) that represents the room of the Name
    /// </summary>
    /// <param name="text"></param>
    public void SetRoomNameText(string text)
    {
        RoomName = text.Split('@')[0];
        Password = text.Split('@')[1];
        roomNameText.text = RoomName;
    }
}
