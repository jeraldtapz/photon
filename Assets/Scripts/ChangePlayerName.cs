﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChangePlayerName : MonoBehaviour
{
    /// <summary>
    /// Text UI from InputField that shows what the user typed
    /// </summary>
    [SerializeField]
    private Text changeNameText;

    /// <summary>
    /// The UI block for changing the name
    /// </summary>
    [SerializeField]
    private GameObject changeNameProperties;
    
    /// <summary>
    /// The constant string to store the string key for the Name of the player stored locally
    /// </summary>
    public static string NamePlayerPrefsKey = "PlayerName";


    

    /// <summary>
    /// Method attached to the Done button when you're changing the name
    /// </summary>
    public void OnChangeNameDoneButton()
    {
        PlayerLayoutGroup.Instance.ChangePlayerListingName(PhotonNetwork.playerName, changeNameText.text);
        string nameTemp = changeNameText.text;
        PlayerNetwork.Instance.Name = nameTemp;
        PlayerPrefs.SetString(NamePlayerPrefsKey, nameTemp);
        PhotonNetwork.playerName = nameTemp;
        LobbyManager.Instance.ChangePlayerNameOnLobby(PhotonNetwork.playerName);
        RoomManager.Instance.ChangePlayerNameOnRoom(PhotonNetwork.playerName);
        OnChangeNameCancelButton();
    }

    /// <summary>
    /// Method attached to the Name Button
    /// Used to show the change name UI
    /// </summary>
    public void OnChangeNameButton()
    {
        changeNameProperties.SetActive(true);
        changeNameProperties.transform.SetAsLastSibling();
    }

    /// <summary>
    /// Gets called when the X button is pressed when changing the name
    /// </summary>
    public void OnChangeNameCancelButton()
    {
        changeNameProperties.transform.SetAsFirstSibling();
        changeNameProperties.SetActive(false);
    }

}
